const quiz = [
    { name: "Superman",realName: "Clark Kent" },
    { name: "Wonderwoman",realName: "Dianna Prince" },
    { name: "Batman",realName: "Bruce Wayne" },
];

// View Object
const view = {
score: document.querySelector('#score strong'),
question: document.getElementById('question'),
result: document.getElementById('result'),
info: document.getElementById('info'),
start: document.getElementById('start'),
response: document.querySelector('#response'),
timer: document.querySelector('#timer strong'),
render(target,content,attributes) {
for(const key in attributes) {
target.setAttribute(key, attributes[key]);
}
target.innerHTML = content;
},
show(element){
    element.style.display = 'block';
    },
hide(element){
    element.style.display = 'none';
},
resetForm(){
    this.response.answer.value = '';
    this.response.answer.focus();
},

setup(){
this.show(this.question);
this.show(this.response);
this.show(this.result);
this.hide(this.start);
this.render(this.score,game.score);
this.render(this.result,'');
this.render(this.info,'');
this.resetForm();
},
teardown(){
this.hide(this.question);
this.hide(this.response);
this.show(this.start);
}
};

/*  
render() function that is used to update the content of an element on the page. 
This function has three parameters: the first is the element that displays the content, 
the second is for the content it’s to be updated with, 
and the last is an object of any HTML attributes that can be added to the element.

The function loops through any attributes provided as the third argument, 
and uses the setAttribute() method to update them to the values provided. 
It then uses the innerHTML property to update the HTML with the content provided.
*/


// Game Object
const game = {
start(quiz){
    this.score = 0;
    this.questions = [...quiz];
    view.setup();
    this.secondsRemaining = 20;
    this.timer = setInterval( this.countdown , 1000 );
    this.ask();
  },
 
  countdown() { // every time its called it decreases the seconds remaining
    game.secondsRemaining--;
    view.render(view.timer,game.secondsRemaining);
    if(game.secondsRemaining < 0) {
        game.gameOver();
    }
},

  ask(name){
    if(this.questions.length > 0) {
      this.question = this.questions.pop(); //the pop() method is used to remove the last element of the array and assign it to this.question
      const question = `What is ${this.question.name}'s real name?`;
      view.render(view.question,question); // calls the render function from the view class/object. passes in the view objects question key and the newly made question variable
    }
    else {
      this.gameOver();
    }
  },
  
  check(event){
    event.preventDefault(); //prevent submit button from leading anywhere
    const response = view.response.answer.value; //getting the value stored in <input>
    const answer = this.question.realName;
    if(response === answer){
      view.render(view.result,'Correct!',{'class':'correct'}); //updates the page elements by changing the result text to correct and the css class of result to correct 
      this.score++;
      view.render(view.score,this.score);
    } else {
      view.render(view.result,`Wrong! The correct answer was ${answer}`,{'class':'wrong'});
    }
    view.resetForm();
    this.ask();
  },
  
  gameOver(){
    view.render(view.info,`Game Over, you scored ${this.score} point${this.score !== 1 ? 's' : ''}`);
    view.teardown();
    clearInterval(this.timer);
  }
}

view.start.addEventListener('click', () => game.start(quiz), false);
view.response.addEventListener('submit', (event) => game.check(event), false);
view.hide(view.response);

/*
The ask(), check() and gameOver() functions are defined at the end of the body of the start() function. 
They need to be placed inside the start() function as nested functions, as this gives them access to any variables defined inside the start() function's scope. 
Because they are defined using function declarations, they are hoisted, so they can be defined after they are invoked.
*/
